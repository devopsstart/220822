FROM php:7.2-fpm-buster

ENV DB_HOST=db
ENV DB_DATABASE=travellist
ENV DB_USERNAME=admin
ENV DB_PASSWORD=admin

ENV PHP_USER_ID=33

# Install PHP ext and unzip
COPY --from=mlocati/php-extension-installer /usr/bin/install-php-extensions /usr/local/bin/
RUN install-php-extensions bcmath pdo_mysql && \
    apt update && \
    apt install -y unzip && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Change workdir
WORKDIR /app

# Install Composer1 latest
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
php -r "if (hash_file('sha384', 'composer-setup.php') === '55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" && \
php composer-setup.php --version=1.10.26 && \
php -r "unlink('composer-setup.php');"


COPY composer.json composer.lock .
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN php -d memory_limit=-1 composer.phar install


# Copy app files 
COPY . .
COPY .env.example .env



RUN php artisan key:generate && php artisan migrate

